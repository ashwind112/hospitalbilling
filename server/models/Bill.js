const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BillSchema= new Schema({
    BillDate:{
        type:Date,
        required:[true,"Bill must have bill date"]
    },
    AdmissionFee:{
        type:Number,
        required:[true, "Adminssion fee is mandatory"]
    },
    BedCharges:{
        GeneralWard:{
            Multiplier:{
                type:Number,
                required:[true,"Number of days is mandatory"]
            },
            Amount:{
                type:Number,
                required:[true,"Amount is mandatory"]
            }
        },
        ICU:{
            Multiplier:{
                type:Number,
                required:[true,"Number of days is mandatory"]
            },
            Amount:{
                type:Number,
                required:[true,"Amount is mandatory"]
            }
        },
        SpecialRoom:{
            Multiplier:{
                type:Number,
                required:[true,"Number of days is mandatory"]
            },
            Amount:{
                type:Number,
                required:[true,"Amount is mandatory"]
            }
        },
        Casualty:{
            Multiplier:{
                type:Number,
                required:[true,"Number of days is mandatory"]
            },
            Amount:{
                type:Number,
                required:[true,"Amount is mandatory"]
            }
        }
    },
    DoctorCharges:{
        GeneralWard:{
            Multiplier:{
                type:Number,
                required:[true,"Number of days is mandatory"]
            },
            Amount:{
                type:Number,
                required:[true,"Amount is mandatory"]
            }
        },
        ICU:{
            Multiplier:{
                type:Number,
                required:[true,"Number of days is mandatory"]
            },
            Amount:{
                type:Number,
                required:[true,"Amount is mandatory"]
            }
        },
        SpecialRoom:{
            Multiplier:{
                type:Number,
                required:[true,"Number of days is mandatory"]
            },
            Amount:{
                type:Number,
                required:[true,"Amount is mandatory"]
            }
        },
        Casualty:{
            Multiplier:{
                type:Number,
                required:[true,"Number of days is mandatory"]
            },
            Amount:{
                type:Number,
                required:[true,"Amount is mandatory"]
            }
        }
    },
    NursingCharges:{
        GeneralWard:{
            Multiplier:{
                type:Number,
                required:[true,"Number of days is mandatory"]
            },
            Amount:{
                type:Number,
                required:[true,"Amount is mandatory"]
            }
        },
        ICU:{
            Multiplier:{
                type:Number,
                required:[true,"Number of days is mandatory"]
            },
            Amount:{
                type:Number,
                required:[true,"Amount is mandatory"]
            }
        },
        SpecialRoom:{
            Multiplier:{
                type:Number,
                required:[true,"Number of days is mandatory"]
            },
            Amount:{
                type:Number,
                required:[true,"Amount is mandatory"]
            }
        },
        Casualty:{
            Multiplier:{
                type:Number,
                required:[true,"Number of days is mandatory"]
            },
            Amount:{
                type:Number,
                required:[true,"Amount is mandatory"]
            }
        }
    },
    MonitorCharges:{
        Multiplier:{
            type:Number,
            required:[true,"Number of days is mandatory"]
        },
        Amount:{
            type:Number,
            required:[true,"Amount is mandatory"]
        }
    },
    ProcedureCharges:{
        CathetorCharges:{
            Multiplier:{
                type:Number
            },
            Amount:{
                type:Number
            }
        },
        SyringePump:{
            Multiplier:{
                type:Number
            },
            Amount:{
                type:Number
            }
        },
        CentralLine:{
            Amount:{
                type:Number
            }
        },
        RTProcedure:{
            Amount:{
                type:Number
            }
        },
        Dressing:{
            Multiplier:{
                type:Number
            },
            Amount:{
                type:Number
            }
        }
    },
    OxygenCharges:{
        Multiplier:{
            type:Number,
            required:[true,"Number of days is mandatory"]
        },
        Amount:{
            type:Number,
            required:[true,"Amount is mandatory"]
        }
    },
    NebulizationCharges:{
        Multiplier:{
            type:Number,
            required:[true,"Number of days is mandatory"]
        },
        Amount:{
            type:Number,
            required:[true,"Amount is mandatory"]
        }
    },
    VentilatorCharges:{
        Multiplier:{
            type:Number,
            required:[true,"Number of days is mandatory"]
        },
        Amount:{
            type:Number,
            required:[true,"Amount is mandatory"]
        }
    },
    ECGCharges:{
        Multiplier:{
            type:Number,
            required:[true,"Number of days is mandatory"]
        },
        Amount:{
            type:Number,
            required:[true,"Amount is mandatory"]
        }
    },
    BSLCharges:{
        Multiplier:{
            type:Number,
            required:[true,"Number of days is mandatory"]
        },
        Amount:{
            type:Number,
            required:[true,"Amount is mandatory"]
        }
    },
    XRayCharges:{
        Multiplier:{
            type:Number,
            required:[true,"Number of days is mandatory"]
        },
        Amount:{
            type:Number,
            required:[true,"Amount is mandatory"]
        }
    },
    AirBedCharges:{
        Multiplier:{
            type:Number,
            required:[true,"Number of days is mandatory"]
        },
        Amount:{
            type:Number,
            required:[true,"Amount is mandatory"]
        }
    },
    BloodTransfusion:{
        Multiplier:{
            type:Number,
            required:[true,"Number of days is mandatory"]
        },
        Amount:{
            type:Number,
            required:[true,"Amount is mandatory"]
        }
    },
    PlateletsCharges:{
        Multiplier:{
            type:Number,
            required:[true,"Number of days is mandatory"]
        },
        Amount:{
            type:Number,
            required:[true,"Amount is mandatory"]
        }
    },
    TappingCharges:{
        Multiplier:{
            type:Number,
            required:[true,"Number of days is mandatory"]
        },
        Amount:{
            type:Number,
            required:[true,"Amount is mandatory"]
        }
    },
    BIPAPCharges:{
        Multiplier:{
            type:Number,
            required:[true,"Number of days is mandatory"]
        },
        Amount:{
            type:Number,
            required:[true,"Amount is mandatory"]
        }
    },
    IntubationCharges:{
        Multiplier:{
            type:Number,
            required:[true,"Number of days is mandatory"]
        },
        Amount:{
            type:Number,
            required:[true,"Amount is mandatory"]
        }
    },
    VisitingConsultantionCharges:{
        Doctors:[{
            type:String
        }],
        Amount:[{
            type:Number
        }]
    },
    EmergencyMedicalManagementCharges:{
        Amount:{
            type:Number,
            required:[true,"Amount is mandatory"]
        }
    },
    Discount:{
        type:Number
    }
})

const Bill = mongoose.model('bill',BillSchema);

module.exports = Bill;