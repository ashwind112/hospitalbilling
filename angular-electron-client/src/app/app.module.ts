import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule, NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { PatientsComponent } from './patients/patients.component';
import { AdmittedPatientsComponent } from './admitted-patients/admitted-patients.component';
import { ViewPatientComponent } from './view-patient/view-patient.component';
import {PatientUtilityService} from './patient-utility.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ViewAllPatientsComponent } from './view-all-patients/view-all-patients.component';
import { BillModalContent } from './admitted-patients/bill-modal';

@NgModule({
  declarations: [
    AppComponent,
    PatientsComponent,
    AdmittedPatientsComponent,
    ViewPatientComponent,
    ViewAllPatientsComponent,
    BillModalContent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule.forRoot(),
    FormsModule,
    NgxSpinnerModule,
  ],
  providers: [PatientUtilityService],
  bootstrap: [AppComponent],
  entryComponents:[
    BillModalContent
  ]
  
})
export class AppModule { }
