import { Component, OnInit } from '@angular/core';
import { PatientUtilityService } from '../patient-utility.service';
import { Patient } from '../../model/Patients';

@Component({
  selector: 'app-view-all-patients',
  templateUrl: './view-all-patients.component.html',
  styleUrls: ['./view-all-patients.component.css']
})
export class ViewAllPatientsComponent implements OnInit {

  constructor(private patientUtilityService: PatientUtilityService) { }
  AllPatients:Patient[];
  selectedPatient:Patient;
  isCollapsed:Boolean;
  lastdoa:Date;

  ngOnInit() {
    this.getAllPatients();
    
    //this.lastdoa=new(Date);
  }

  getAllPatients(){
    this.patientUtilityService.getPatients().subscribe((patients)=>{
      this.AllPatients=patients;
      this.AllPatients.sort((patient1,patient2) =>{
        return (patient1.FirstName.localeCompare(patient2.FirstName));
      });
    });
  }


}
