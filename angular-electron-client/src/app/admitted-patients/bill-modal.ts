import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, ModalDismissReasons, NgbDropdown,NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { Patient } from '../../model/Patients';
import { Bill } from '../../model/Bill';
import { PatientUtilityService } from '../patient-utility.service';



@Component({
    selector:'app-bill-modal',
    templateUrl:'./bill-modal.html',
    styleUrls: ['./bill-modal.css']
  })
  export class BillModalContent implements OnInit{

    @Input() title = `Information`;
    @Input() billingPatient:Patient;

    billForPatient:Bill=new Bill();;

    totalICU:number=0;
    totalSpecial:number=0;
    totalGeneralWard:number=0;
    totalCasualty:number=0;
    totalProcedure:number=0;
    totalBill:number=0;

    constructor(public activeModal:NgbActiveModal,private modalService: NgbModal,private patientUtitlityService:PatientUtilityService) { }

    ngOnInit(){
      console.log(this.billForPatient);
      this.title= this.billingPatient.FirstName + " Patient's Bill";
      this.totalSpecial=    (this.billForPatient.DoctorCharges.SpecialRoom.Amount * this.billForPatient.DoctorCharges.SpecialRoom.Multiplier)
                         +  (this.billForPatient.BedCharges.SpecialRoom.Amount * this.billForPatient.BedCharges.SpecialRoom.Multiplier)
                         +  (this.billForPatient.NursingCharges.SpecialRoom.Amount * this.billForPatient.NursingCharges.SpecialRoom.Multiplier);
    }

    SaveBill(){
      console.log(this.billForPatient);
      this.patientUtitlityService.setBillForPatient(this.billingPatient._id,this.billForPatient).subscribe(
      res =>{
        console.log(res);
      },
      err =>{
        console.log("Error Occured" + err);
      });
    }

    calculateTotalSpecial(){
      this.totalSpecial=    (this.billForPatient.DoctorCharges.SpecialRoom.Amount * this.billForPatient.DoctorCharges.SpecialRoom.Multiplier)
                         +  (this.billForPatient.BedCharges.SpecialRoom.Amount * this.billForPatient.BedCharges.SpecialRoom.Multiplier)
                         +  (this.billForPatient.NursingCharges.SpecialRoom.Amount * this.billForPatient.NursingCharges.SpecialRoom.Multiplier);
      this.calculateBillTotal();
    }
    calculateTotalGeneral(){
      this.totalGeneralWard=    (this.billForPatient.DoctorCharges.GeneralWard.Amount * this.billForPatient.DoctorCharges.GeneralWard.Multiplier)
                         +  (this.billForPatient.BedCharges.GeneralWard.Amount * this.billForPatient.BedCharges.GeneralWard.Multiplier)
                         +  (this.billForPatient.NursingCharges.GeneralWard.Amount * this.billForPatient.NursingCharges.GeneralWard.Multiplier);

      this.calculateBillTotal();
    }
    calculateTotalICU(){
      this.totalICU=    (this.billForPatient.DoctorCharges.ICU.Amount * this.billForPatient.DoctorCharges.ICU.Multiplier)
                         +  (this.billForPatient.BedCharges.ICU.Amount * this.billForPatient.BedCharges.ICU.Multiplier)
                         +  (this.billForPatient.NursingCharges.SpecialRoom.Amount * this.billForPatient.NursingCharges.ICU.Multiplier);

      this.calculateBillTotal();                   
    }
    calculateTotalCasulaty(){
      this.totalCasualty=    (this.billForPatient.DoctorCharges.Casualty.Amount * this.billForPatient.DoctorCharges.Casualty.Multiplier)
                         +  (this.billForPatient.BedCharges.Casualty.Amount * this.billForPatient.BedCharges.Casualty.Multiplier)
                         +  (this.billForPatient.NursingCharges.Casualty.Amount * this.billForPatient.NursingCharges.Casualty.Multiplier);
    }

    calculateTotalProcedure(){
      this.totalProcedure=  (this.billForPatient.ProcedureCharges.CathetorCharges.Amount * this.billForPatient.ProcedureCharges.CathetorCharges.Multiplier)
                         +  (this.billForPatient.ProcedureCharges.SyringePump.Amount * this.billForPatient.ProcedureCharges.SyringePump.Multiplier) 
                         +  (this.billForPatient.ProcedureCharges.CentralLine.Amount)
                         +  (this.billForPatient.ProcedureCharges.RTProcedure.Amount)
                         +  (this.billForPatient.ProcedureCharges.Dressing.Amount * this.billForPatient.ProcedureCharges.Dressing.Multiplier);
      
    this.calculateBillTotal();
    }

    calculateBillTotal(){
      this.totalBill=   this.billForPatient.AdmissionFee + this.totalGeneralWard + this.totalICU + this.totalSpecial + this.totalCasualty + this.totalProcedure
                      + (this.billForPatient.MonitorCharges.Amount * this.billForPatient.MonitorCharges.Multiplier)
                      + (this.billForPatient.OxygenCharges.Amount * this.billForPatient.OxygenCharges.Multiplier)
                      + (this.billForPatient.NebulizationCharges.Amount * this.billForPatient.NebulizationCharges.Multiplier)
                      + (this.billForPatient.VentilatorCharges.Amount * this.billForPatient.VentilatorCharges.Multiplier)
                      + (this.billForPatient.ECGCharges.Amount * this.billForPatient.ECGCharges.Multiplier)
                      + (this.billForPatient.BSLCharges.Amount * this.billForPatient.BSLCharges.Multiplier)
                      + (this.billForPatient.XRayCharges.Amount * this.billForPatient.XRayCharges.Multiplier)
                      + (this.billForPatient.AirBedCharges.Amount * this.billForPatient.AirBedCharges.Multiplier)
                      + (this.billForPatient.BloodTransfusion.Amount * this.billForPatient.BloodTransfusion.Multiplier)
                      + (this.billForPatient.PlateletsCharges.Amount * this.billForPatient.PlateletsCharges.Multiplier)
                      + (this.billForPatient.TappingCharges.Amount * this.billForPatient.TappingCharges.Multiplier)
                      + (this.billForPatient.BIPAPCharges.Amount * this.billForPatient.BIPAPCharges.Multiplier)
                      + (this.billForPatient.IntubationCharges.Amount * this.billForPatient.IntubationCharges.Multiplier)
                      + (this.billForPatient.EmergencyMedicalManagementCharges.Amount)
                      - (this.billForPatient.Discount);
                      
    }
}