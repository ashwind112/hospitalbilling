export class GenericMultiplierAmount {
    Multiplier:number;
    Amount:number;

    constructor(){
        this.Multiplier=0;
        this.Amount=0;
    }
}

export class GenericAmount {
    Amount:number;
    constructor(){
        this.Amount=0;
    }
}

export class ProcedureChargesInfo{
    CathetorCharges:GenericMultiplierAmount;
    SyringePump:GenericMultiplierAmount;
    CentralLine:GenericAmount;
    RTProcedure:GenericAmount;
    Dressing:GenericMultiplierAmount;

    constructor(){
        this.CathetorCharges=new GenericMultiplierAmount();
        this.SyringePump=new GenericMultiplierAmount();
        this.CentralLine=new GenericAmount();
        this.RTProcedure=new GenericAmount();
        this.Dressing=new GenericMultiplierAmount();
    }
}

export class BedChargesInfo {
    GeneralWard:GenericMultiplierAmount
    ICU:GenericMultiplierAmount;
    SpecialRoom:GenericMultiplierAmount;
    Casualty:GenericMultiplierAmount;

    constructor(){
        this.GeneralWard=new GenericMultiplierAmount();
        this.ICU=new GenericMultiplierAmount();
        this.SpecialRoom=new GenericMultiplierAmount();
        this.Casualty=new GenericMultiplierAmount();
    }
}

export class DoctorChargesInfo {
    GeneralWard:GenericMultiplierAmount
    ICU:GenericMultiplierAmount;
    SpecialRoom:GenericMultiplierAmount;
    Casualty:GenericMultiplierAmount;

    constructor(){
        this.GeneralWard=new GenericMultiplierAmount();
        this.ICU=new GenericMultiplierAmount();
        this.SpecialRoom=new GenericMultiplierAmount();
        this.Casualty=new GenericMultiplierAmount();
    }
}

export class NursingChargesInfo{
    GeneralWard:GenericMultiplierAmount
    ICU:GenericMultiplierAmount;
    SpecialRoom:GenericMultiplierAmount;
    Casualty:GenericMultiplierAmount;

    constructor(){
        this.GeneralWard=new GenericMultiplierAmount();
        this.ICU=new GenericMultiplierAmount();
        this.SpecialRoom=new GenericMultiplierAmount();
        this.Casualty=new GenericMultiplierAmount();
    }
}

export class VisitingConsultantionCharges{
    Doctors:String[];
    Amount:Number[];
}

export class Bill{
    _id:Number;
    BillDate:Date;
    AdmissionFee:number;
    BedCharges:BedChargesInfo;
    DoctorCharges:DoctorChargesInfo;
    NursingCharges:NursingChargesInfo;
    ProcedureCharges:ProcedureChargesInfo;
    MonitorCharges:GenericMultiplierAmount;
    OxygenCharges:GenericMultiplierAmount;
    NebulizationCharges:GenericMultiplierAmount;
    VentilatorCharges:GenericMultiplierAmount;
    ECGCharges:GenericMultiplierAmount;
    BSLCharges:GenericMultiplierAmount;
    XRayCharges:GenericMultiplierAmount;
    AirBedCharges:GenericMultiplierAmount;
    BloodTransfusion:GenericMultiplierAmount;
    PlateletsCharges:GenericMultiplierAmount;
    TappingCharges:GenericMultiplierAmount;
    BIPAPCharges:GenericMultiplierAmount;
    IntubationCharges:GenericMultiplierAmount;
    VisitingConsultantionCharges:VisitingConsultantionCharges;
    EmergencyMedicalManagementCharges:GenericAmount;
    Discount:number;

    constructor(){
        this.BillDate=new Date();
        this.AdmissionFee = 0;
        this.BedCharges=new BedChargesInfo();
        this.DoctorCharges=new DoctorChargesInfo();
        this.NursingCharges=new NursingChargesInfo();
        this.ProcedureCharges=new ProcedureChargesInfo();
        this.MonitorCharges=new GenericMultiplierAmount();
        this.OxygenCharges= new GenericMultiplierAmount();
        this.NebulizationCharges=new GenericMultiplierAmount();
        this.VentilatorCharges=new GenericMultiplierAmount();
        this.ECGCharges=new GenericMultiplierAmount();
        this.BSLCharges=new GenericMultiplierAmount();
        this.XRayCharges=new GenericMultiplierAmount();
        this.AirBedCharges=new GenericMultiplierAmount();
        this.BloodTransfusion=new GenericMultiplierAmount();
        this.PlateletsCharges=new GenericMultiplierAmount();
        this.TappingCharges=new GenericMultiplierAmount();
        this.BIPAPCharges=new GenericMultiplierAmount();
        this.IntubationCharges=new GenericMultiplierAmount();
        this.VisitingConsultantionCharges=new VisitingConsultantionCharges();
        this.EmergencyMedicalManagementCharges=new GenericAmount();
        this.Discount=0;
    }
}
