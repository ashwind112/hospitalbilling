const { app, BrowserWindow } = require('electron')

let mainWindow,
    loadingScreen,
    windowParams = {
        width: 1000,
        height: 700,
        show: false
    };

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    
    backgroundColor: '#ffffff',
    icon: `file://${__dirname}/dist/assets/logo.png`,
  })



  mainWindow.loadURL(`file://${__dirname}/dist/index.html`)
  //win.loadURL('http://localhost:4200/')
  //// uncomment below to open the DevTools.
   //win.webContents.openDevTools()
   mainWindow.webContents.on('did-finish-load', () => {
   mainWindow.show();

   if (loadingScreen) {
            let loadingScreenBounds = loadingScreen.getBounds();
            mainWindow.setBounds(loadingScreenBounds);
            loadingScreen.close();
            mainWindow.maximize(true);
        }
   });

      // Open the DevTools.
      //mainWindow.webContents.openDevTools();

      // Emitted when the window is closed.
      mainWindow.on('closed', function() {
          // Dereference the window object, usually you would store windows
          // in an array if your app supports multi windows, this is the time
          // when you should delete the corresponding element.
          mainWindow = null;
      })
  // Event when the window is closed.

}

function createLoadingScreen() {
    loadingScreen = new BrowserWindow({parent: mainWindow});
    loadingScreen.loadURL('file://' + __dirname + '/dist/loading.html');
    loadingScreen.on('closed', () => loadingScreen = null);
    loadingScreen.webContents.on('did-finish-load', () => {
        loadingScreen.show();
    });
}

// Create window on electron intialization
app.on('ready', () => {
  createLoadingScreen();
  createWindow();
});

// Quit when all windows are closed.
app.on('window-all-closed', function () {

  // On macOS specific close process
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // macOS specific close process
  if (mainWindow === null) {
    createWindow()
  }
})
